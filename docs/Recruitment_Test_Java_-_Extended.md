# Recruitment Test Java (Extended) - Spring Boot - Webservice - Multithread - API - JPA
Ziel ist es, News Beiträge in XML Format über einen Webservice einzulesen und den Inhalt in einer Datenbank zu speichern.
Zudem sollten die Dokumente beim Abpeichern mittels eines bestehenden Service analysiert und die Analyseresultate (extrahierte Entitäten) gespeichert werden.

1. XML File über Webservice abspeichern und auslesen.
2. Titel und Beschreibungstext über Redlink SDK analysieren.
3. Analyseresultate zum Dokument speichern.
4. Asynchrone Analyse + Statuswebservice für Dokumente (No Doc, In Progress, Done) + passendem HTTP Status.
6. "Suche" nach Dokumenten mit bestimmten Entitäten (Volltext) 

## Dabei soll beachtet/verwendet werden:

1. Git und Maven
2. Service Orientierted Architektur
3. Rest-full Webservices
3. Möglichst mit Spring-Boot on-board Mittlen umsetzen.

## Beispiel Interaction

```
curl -XPOST http://localhost:8080/xml -d '<article><title>Title</title><description>Description</description></article>'  -H 'Content-Type:application/xml'

=> CREATED: Location: http://localhost:8080/xml/123

curl -XGET http://localhost:8080/xml/123 -H 'Accept:application/json'

=> OK: <article><id>123</id><title>Title</title><description>Description</description></article>

curl -XGET http://localhost:8080/xml/123/status -H 'Accept:application/json'

=> OK: <status>Accpeted</status>

curl -XGET http://localhost:8080/xml?entity="Abc" -H 'Accept:application/json'
```

## Links

1. Aufsetzen einer Spring Boot Applikation mit Maven
    * https://spring.io/guides/gs/spring-boot/

2. Redlink Java SDK
    * https://github.com/redlink-gmbh/redlink-java-sdk
    
3. Redlink Key
    * `xvPN2sWDNzJKWpuWBe7rnUTRNYzBoQ8eff358f13`
4. Daten
    * `./data`