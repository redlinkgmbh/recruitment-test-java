# Recruitment Test Java - Vind - Spring Boot - Webservice - API - Swagger
Ziel ist es, News Beiträge in JSON Format über einen Webservice einzulesen und den Inhalt in Solr zu speichern.
Ebenfalls soll über einen Webservice eine kombinierte Volltext/Filtersuche angeboten werden. Die Suche sollte
eine geordnete Liste von Suchergebnissen sowie Kategorie und Tag Facetten liefern.

1. Spring Boot Projekt anlegen
2. JSON Data über Webservice abspeichern/auslesen.
3. Titel, Beschreibungstext, Tags und Kategorie in Sorl speichern.
4. Suchservice für Volltextsuche und Filter (tags + category)
5. Index und Suchservice testen
6. Sucherservice via Webservice exponieren

## Dabei soll beachtet/verwendet werden:

1. Git und Maven/Gradle
2. Service Orientierted Architektur
3. Beschreibung der API via Swagger/OpenApi
4. Test des Suchservice mit eingebettetem Solr
5. Demonstration mit Solr in Docker

## Beispiel Interaction

```
curl -XPOST http://localhost:8080/article -d '{...}'  -H 'Content-Type:application/json'

=> CREATED: Location: http://localhost:8080/article/123

curl -XGET http://localhost:8080/article/123 -H 'Accept:application/json'

=> OK: {...}

curl -XGET http://localhost:8080/search?text=abc&tag=t1&tag=t2&category=c1 -H 'Accept:application/json'

=> OK: {result:[{},{}],facets:..}
```

## Links

1. Aufsetzen einer Spring Boot Applikation mit Maven
    * https://spring.io/guides/gs/spring-boot/

2. Vind Lib
    * https://github.com/RBMHTechnology/vind
    
3. Vind Solr Docker
    * https://github.com/redlink-gmbh/vind-solr-server
    
3. Swagger Editor
    * https://editor.swagger.io/
