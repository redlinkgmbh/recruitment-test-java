# Recruitment Test Java (Extended) - Spring Boot - Webservice - Multithread - API - JPA
Ziel ist es, News Beiträge in einen Webservice einzulesen und den Inhalt in einer Datenbank zu speichern.
Zudem sollten die Dokumente beim Abpeichern mittels eines bestehenden Service analysiert und die Analyseresultate (extrahierte Entitäten) gespeichert werden.

0. Aufsetzen eines Spring Boot Pojekts
1. CRUD eines Nachrichtenartikels mit Titel, Datum, Beschreibungstext, Tags (Json)
2. Speichern der Daten in einer Datenbank
3. Beschreibungstext über Redlink SDK analysieren.
4. Analyseresultate zum Dokument speichern (Tags).
5. Exponierung des Services als Rest Webservice
6. Testen des Services (Mocken des Repositories)
7. Asynchrone Analyse + Statuswebservice für Dokumente (No Doc, In Progress, Done) + passendem HTTP Status.

## Dabei soll beachtet/verwendet werden:

1. Git und Maven
2. Service Orientierted Architektur
3. Rest-full Webservices
3. Möglichst mit Spring-Boot on-board Mittlen umsetzen.

## Beispiel Interaction

```
curl -XPOST http://localhost:8080/article -d '{"title":"T","description":"..."}'  -H 'Content-Type:application/json'

=> CREATED: Location: http://localhost:8080/article/123

curl -XGET http://localhost:8080/article/123 -H 'Accept:application/json'

=> OK: {...}

curl -XGET http://localhost:8080/article/123/status -H 'Accept:application/json'

=> OK: {"status":"DONE"}
```

## Links

1. Aufsetzen einer Spring Boot Applikation mit Maven
    * https://spring.io/guides/gs/spring-boot/

2. Redlink Java SDK
    * https://github.com/redlink-gmbh/redlink-java-sdk
    
3. Redlink Key
    * `xvPN2sWDNzJKWpuWBe7rnUTRNYzBoQ8eff358f13`
